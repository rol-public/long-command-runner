# frozen_string_literal: true

# This lib aims to follow a terminal command that is long to execute.
# It will monitor the CPU usage and the progression of the program (given by its output).
module LCR; end

require 'long-command-runner/version'
require 'long-command-runner/container'
require 'long-command-runner/line_reader'
require 'long-command-runner/runner'
