#  frozen_string_literal: true

module LCR
  # Version of the library.
  VERSION = '0.3.0'
end
