#  frozen_string_literal: true

require 'open3'

module LCR
  # This class aims to contain the runing script.
  class Container
    # The expected Language environment:
    ENVIRONMENT = { 'LANGUAGE' => 'en' }.freeze

    # Get the standard input IO of the process.
    #
    # @note Be aware that it may be closed by the process (on  exit), or by you.
    #   We don't monitor those states here.
    #
    # @return [IO]
    attr_reader :stdin

    # Get the standard output IO of the process.
    #
    # @note Be aware that it may be closed by the process (on  exit), or by you.
    #   We don't monitor those states here.
    #
    # @return [IO]
    attr_reader :stdout

    # Get the standard error IO of the process.
    #
    # @note Be aware that it may be closed by the process (on  exit), or by you.
    #   We don't monitor those states here.
    #
    # @return [IO]
    attr_reader :stderr

    # Initializer takes the command as a plain string.
    # it imediatly launch the command
    # @param [String] command The command to execute.
    # @param [Hash] opts The option to pass to Open3.popen3.
    def initialize(command, opts = {})
      safe_command = quote_sanitize(command)
      @stdin, @stdout, @stderr, @wait_thr = Open3.popen3(ENVIRONMENT, "bash -c \"#{safe_command}\"", opts)
    end

    # Is the last launched command is still running.
    def running?
      @wait_thr.alive?
    end

    # Get the status of the process without blocking.
    #
    # @return [Process:Status, nil] The exit status of the process if it is finished.
    #   if the Process isn't finished it return nil.
    def status
      return nil if running?

      @wait_thr.value
    end

    # Wait and return the process exit status.
    # This method is blocking until the process if finished.
    #
    # @return [Process::Status]
    def wait
      @wait_thr.value
    end

    # Return the pid of the process
    def pid
      @wait_thr.pid
    end

    def children
      get_children = lambda do |pid|
        `pgrep -P #{pid}`.split("\n").map { |c_pid| [c_pid.to_i, get_children.call(c_pid)] }.to_h
      end
      get_children.call(pid)
    end

    private

    def quote_sanitize(cmd)
      cmd.gsub(/(["])/, '\\"')
    end
  end
end
