#!/usr/bin/env ruby
#  frozen_string_literal: true

# This test is aim to create border effect and output that will be read by
# the Runner class.
# It's only perpose is to test Runner class functionalities.
#
# Without argument this command runs correctly, and display a progress
# indicator on the STDOUT, and after few deci-seconds will display an error
# on STDERR for the Runner to catch.

require 'securerandom'

STDOUT.sync = true
STDOUT.puts 'Hello'

# get current process user + system time
def u_s_time
  times = Process.times
  times.utime + times.stime
end

# This function is just to run the CPU during 100ms
def iteration_run
  some_cumulator = 0
  before = u_s_time
  loop do
    some_cumulator += SecureRandom.rand + SecureRandom.rand
    now = u_s_time
    break if now - before >= 0.1
  end
  some_cumulator
end

iterations = ARGV.length != 1 ? 12 : 620
current = 1

STDERR.write " 0% - starting\r"
while current < iterations
  progress = 100.0 * current.to_f / iterations.to_f
  iteration_run
  STDERR.write " #{progress.truncate 2}% - running   \r"

  current += 1
end

warn ' 100% job done      '
exit 0
