#!/usr/bin/env ruby
#  frozen_string_literal: true

require 'open3'
# This test script is made to test Container class functionalities.
#
# It doesn't take any argument.

STDOUT.puts 'Hello'

_, _, wt_thr = Open3.popen2('sleep 0.1')

unless select([STDIN], [], [], 0.1)
  warn 'TIMEOUT!'
  wt_thr.join
  exit 1
end

said = STDIN.gets

exit 2 if said.nil?

said.chomp!

STDOUT.puts said
warn "Error channel: #{said}"

wt_thr.join
exit 0
