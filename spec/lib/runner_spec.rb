#  frozen_string_literal: true

def runner_stdout(*args)
  iterations = args.length != 1 ? 12 : 620
  result = ['Hello']

  current = 1
  result << ' 0% - starting'
  while current < iterations
    progress = (100.0 * current) / iterations
    result << " #{progress.truncate 2}% - running   "
    return result if current == 4 && args.length > 1

    current += 1
  end
  result << ' 100% job done      '
end

def runner_stderr(*args)
  return ['Error log !', 'Too many argument, I crash!'] if args.length > 1

  ['Error log !']
end

def runner_exitstatus(*args)
  return 1 if args.length > 1

  0
end

def runner_successful?(*args)
  return false if args.length > 1

  true
end

TEST_SCRIPT = './spec/data/bin/script.rb'
TEST_SCRIPT_STDERR = './spec/data/bin/script_stderr.rb'

RSpec.describe LCR::Runner do
  let(:args) { [] }
  let(:command) { args.empty? ? TEST_SCRIPT : TEST_SCRIPT.concat(' ', args.join(' ')) }
  subject(:runner) do
    LCR::Runner.new(command)
  end
  it { is_expected.to be }

  context 'short program' do
    describe 'async execution' do
      context 'launched' do
        before(:each) do
          @stdout = []
          @stderr = []
          @runner = LCR::Runner.new(TEST_SCRIPT) do |out_str, err_str|
            @stdout << out_str unless out_str.nil?
            @stderr << err_str unless err_str.nil?
          end
          @runner.launch
        end

        subject { @runner }

        it 'should have a pid to give' do
          expect(@runner.pid).to be_an_instance_of(Integer)
        end

        it 'should have launched the process' do
          expect(@runner).to be_launched
          expect { Process.kill(0, @runner.pid) }.not_to raise_error
        end

        it 'should capture stdout while running' do
          sleep 0.1 until @stdout.length >= 1
          old_len = @stdout.length
          first_stdout = @stdout.dup
          sleep 0.1 until @stdout.length > old_len
          expect(@stdout).not_to eq(first_stdout)
          is_expected.to be_running
        end

        it 'should capture stderr' do
          sleep 0.1 until @stdout.length >= 7
          expect(@stderr).not_to be_empty
          expect(@runner.output_error).not_to be_empty
        end

        it { is_expected.to be_running }

        it 'can signal process' do
          is_expected.to be_running
          @runner.kill(15)
          sleep 0.1
          is_expected.not_to be_running
        end

        it 'can wait then end' do
          is_expected.to be_running
          @runner.wait
          is_expected.not_to be_running
        end

        it 'shouldn\'t have a status' do
          expect(@runner.status).not_to be
        end

        describe '#tms' do
          subject { @runner.tms }
          it { is_expected.not_to be }
        end

        describe '#time_user' do
          subject { @runner.time_user }
          it { is_expected.not_to be }
        end

        describe '#time_sys' do
          subject { @runner.time_sys }
          it { is_expected.not_to be }
        end

        describe '#progress' do
          subject { @runner.progress }
          it { is_expected.to be_an_instance_of(Float) }
          it { is_expected.to be_between(0.0, 100.0) }
        end
      end
    end

    %w[valid invalid].each do |arg_state|
      describe "after #{arg_state} execution" do
        before(:context) do
          @stdout = []
          @stderr = []
          @args = arg_state == 'valid' ? [] : %w[1 2]
          @command = @args.empty? ? TEST_SCRIPT : (TEST_SCRIPT + ' ' + @args.join(' '))
          @runner = LCR::Runner.new(@command) do |out_str, err_str|
            @stdout << out_str unless out_str.nil?
            @stderr << err_str unless err_str.nil?
          end
          @runner.launch
          @runner.wait
        end

        describe '#status' do
          subject(:status) { @runner.status }
          it { is_expected.to be_an_instance_of(Process::Status) }

          describe '#success?' do
            subject { status.success? }
            it { is_expected.to eq(runner_successful?(*@args)) }
          end

          describe '#exitstatus' do
            subject { status.exitstatus }
            it { is_expected.to eq(runner_exitstatus(*@args)) }
          end
        end

        describe 'stdout capture' do
          it 'should have captured all' do
            expect(@runner.output).to eq(runner_stdout(*@args).join("\n"))
          end
          it 'should have transmited all' do
            expect(@stdout).to eq(runner_stdout(*@args))
          end
        end

        describe 'stderr capture' do
          it 'should have captured all' do
            expect(@runner.output_error).to eq(runner_stderr(*@args).join("\n"))
          end
          it 'should have transmited all' do
            expect(@stderr).to eq(runner_stderr(*@args))
          end
        end

        describe '#tms' do
          subject(:tms) { @runner.tms }

          it { is_expected.to be_an_instance_of(Benchmark::Tms) }

          describe '#real' do
            subject { tms.real }
            it { is_expected.to be_an_instance_of(Float) }

            if arg_state == 'valid'
              it { is_expected.to be >= 1 }
            else
              it { is_expected.to be >= 0.4 }
            end
          end
        end

        describe '#time_user' do
          subject { @runner.time_user }
          it { is_expected.to be_an_instance_of(Float) }

          it { is_expected.to be >= 0.0 }
        end

        describe '#time_sys' do
          subject { @runner.time_sys }
          it { is_expected.to be_an_instance_of(Float) }

          it { is_expected.to be >= 0.0 }
        end

        describe '#progress' do
          subject { @runner.progress }
          it { is_expected.to be_an_instance_of(Float) }
          if arg_state == 'valid'
            it { is_expected.to eq(100.0) }
          else
            it { is_expected.to eq((400.0 / 12).truncate(2)) }
          end
        end

        describe '.dead?' do
          subject { LCR::Runner.dead?(@runner.pid) }
          it { is_expected.to be_truthy }
        end

        describe '#kill' do
          subject(:kill_it) { @runner.kill(0) }
          it { is_expected.not_to be }
        end
      end
    end

    describe 'opts[:do_progress_on]' do
      describe 'after valid execution' do
        context 'stderr' do
          before(:context) do
            @runner = LCR::Runner.new(TEST_SCRIPT_STDERR, do_progress_on: 'stderr')
            @runner.launch
            @runner.wait
          end

          describe '#progress' do
            subject { @runner.progress }
            it { is_expected.to be_an_instance_of(Float) }
            it { is_expected.to eq(100.0) }
          end
        end
      end
    end
  end

  context 'inexisting program' do
    let(:command) { './inexisting_cmd.sh' }
    before(:example) do
      @stdout_lines = []
      @stderr_lines = []
      @runner = LCR::Runner.new(command) do |stdout_line, stderr_line|
        @stdout_lines <<= stdout_line unless stdout_line.nil?
        @stderr_lines <<= stderr_line unless stderr_line.nil?
      end
    end
    subject { @runner }
    describe '#new' do
      it { is_expected.to be }
    end

    describe '#launch' do
      subject { runner.launch }

      it { is_expected.not_to be }
      it 'should be already finished' do
        subject
        expect(@runner.running?).to be_falsy
      end
    end

    describe 'stdout_lines' do
      subject do
        @runner.launch
        @runner.wait
        @stdout_lines
      end

      it { is_expected.to be_empty }
    end

    describe 'stderr_lines' do
      subject do
        @runner.launch
        @runner.wait
        @stderr_lines
      end

      it { is_expected.not_to be_empty }
      it { is_expected.to eq(['bash: ./inexisting_cmd.sh: No such file or directory']) }
    end
  end
end
