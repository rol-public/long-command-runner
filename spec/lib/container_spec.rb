#  frozen_string_literal: true

CONTAINER_TEST_SCRIPT = './spec/data/bin/container_test.rb'

RSpec.describe LCR::Container do
  context 'container_test script behavior tests' do
    before(:example) do
      @container = LCR::Container.new(CONTAINER_TEST_SCRIPT)
    end
    subject { @container }

    describe 'timeout after 0.1s' do
      before(:example) { @container.wait }
      it { is_expected.not_to be_running }
      it { expect(@container.status.exitstatus).to eq(1) }
      it { expect(@container.status).not_to be_success }
      it { expect(@container.stderr.read).to eq("TIMEOUT!\n") }
      it { expect(@container.stdout.read).to eq("Hello\n") }
    end

    describe 'sending data to stdin' do
      before(:example) do
        @container.stdin.puts 'Ping'
        @container.wait
      end

      it { is_expected.not_to be_running }
      it { expect(@container.status.exitstatus).to eq(0) }
      it { expect(@container.status).to be_success }
      it { expect(@container.stderr.read).to eq("Error channel: Ping\n") }
      it { expect(@container.stdout.read).to eq("Hello\nPing\n") }
    end

    describe 'closing stdin effect' do
      before(:example) do
        @container.stdin.close
        @container.wait
      end

      it { is_expected.not_to be_running }
      it { expect(@container.status.exitstatus).to eq(2) }
      it { expect(@container.status).not_to be_success }
      it { expect(@container.stderr.read).to be_empty }
      it { expect(@container.stdout.read).to eq("Hello\n") }
    end
  end

  context 'still running' do
    before(:example) do
      @container = LCR::Container.new(CONTAINER_TEST_SCRIPT)
      _hello = @container.stdout.gets # wait for 'Hello'
    end
    after(:example) { @container.wait }

    describe '#running?' do
      subject { @container.running? }

      it { is_expected.to be_truthy }
    end

    describe '#status' do
      subject { @container.status }

      it { is_expected.not_to be }
    end

    describe '#wait' do
      subject(:wait) { @container.wait }

      it { is_expected.to be_an_instance_of(Process::Status) }
      it { is_expected.not_to be_success }
      it { is_expected.to be_exited }
      it 'should not be running after waiting' do
        wait
        expect(@container.running?).to be_falsy
      end
    end

    describe '#pid' do
      subject { @container.pid }

      it { is_expected.to be_an_instance_of(Integer) }
      it 'should be the same as indirect pid value getter' do
        pid = `ps -ef | grep #{Process.pid} | grep con[t]ainer_test.rb`.split("\n").map(&:split)[0][1].to_i
        is_expected.to eq(pid)
      end
    end

    describe '#children' do
      subject { @container.children }

      it { is_expected.to be_an_instance_of(Hash) }
      it 'should be Hash[integer,Hash[integer,...]]' do
        hash = subject
        expect(hash).to be_an_instance_of(Hash)
        expect(hash.keys.length).to be > 0
        expect(hash.keys).to all(be_an(Integer))
        hash.each do |_k1, v1|
          expect(v1).to be_an_instance_of(Hash)
          expect(v1.keys).to all(be_an(Integer))
          expect(v1.length).to be > 0
          v1.each do |_k2, v2|
            expect(v2).to be_an_instance_of(Hash)
            expect(v2.keys).to all(be_an(Integer))
          end
        end
      end
    end

    describe 'channels' do
      it '#stdin must be IO' do
        expect(@container.stdin).to be_an_instance_of(IO)
      end
      it '#stdout must be IO' do
        expect(@container.stdout).to be_an_instance_of(IO)
      end
      it '#stderr must be IO' do
        expect(@container.stderr).to be_an_instance_of(IO)
      end
    end
  end

  context 'not running' do
    before(:example) do
      @container = LCR::Container.new(CONTAINER_TEST_SCRIPT)
      Process.kill(15, @container.pid)
      @container.wait
    end

    describe '#running?' do
      subject { @container.running? }

      it { is_expected.to be_falsy }
    end
  end

  context "command containing usage of \' and \"" do
    before(:context) do
      command = "if [ \"$HOME\" != \"\" ]; then echo \"C'est la fête\"; fi"
      @container = LCR::Container.new(command)
      @container.wait
    end
    subject { @container }
    describe('container') { it { is_expected.not_to be_running } }
    describe 'container.status' do
      it { expect(@container.status.exitstatus).to eq(0) }
      it { expect(@container.status).to be_success }
    end
    describe('container.stderr') { it { expect(@container.stderr.read).to eq('') } }
    describe('container.stdout') { it { expect(@container.stdout.read).to eq("C'est la fête\n") } }
  end
end
