#  frozen_string_literal: true

RSpec.describe LCR::LineReader do
  context '2 inputs streams' do
    before(:example) do
      @pipe0_r, @pipe0_w = IO.pipe
      @pipe1_r, @pipe1_w = IO.pipe
      @line_reader = LCR::LineReader.new([@pipe0_r, @pipe1_r])
      @pipes_messages = [%w[a test messages for pipe 1], %w[a test messages for pipe 2 with more on 2]]
    end
    after(:example) do
      [@pipe0_r, @pipe0_w, @pipe1_r, @pipe1_w].each do |pipe_channel|
        pipe_channel.close unless pipe_channel.closed?
      end
    end

    subject do
      Thread.new do
        @pipes_messages[0].each { |line| @pipe0_w.puts line }
        @pipes_messages[1].each { |line| @pipe1_w.write "#{line}\r" }
        @pipe0_w.close
        @pipe1_w.close
      end
      @line_reader.read
    end

    it { is_expected.to be_an_instance_of(Array) }
    it { is_expected.to eq([6, 10]) }

    describe '#streams' do
      subject { @line_reader.streams }

      it { is_expected.to eq([@pipe0_r, @pipe1_r]) }
    end

    context 'with one closed' do
      subject(:read) do
        @pipe0_w.close
        thr = Thread.new do
          @pipes_messages[1].each { |line| @pipe1_w.puts line }
          @pipe1_w.close
        end
        result = @line_reader.read
        thr.join
        result
      end

      it { is_expected.to be_an_instance_of(Array) }
      it { is_expected.to eq([0, 10]) }

      it '[1] should return read lines for second stream' do
        read
        expect(@line_reader[1]).to eq(@pipes_messages[1])
      end

      it '[0] should be empty' do
        read
        expect(@line_reader[0]).to be_empty
      end
    end

    context 'with no data on one' do
      subject(:read) do
        thr = Thread.new do
          @pipes_messages[1].each { |line| @pipe1_w.puts line }
          sleep 0.1
          @pipe1_w.close
          @pipe0_w.close
        end
        result = @line_reader.read
        thr.join
        result
      end

      it { is_expected.to be_an_instance_of(Array) }
      it { is_expected.to eq([0, 10]) }

      it '[1] should return read lines for second stream' do
        read
        expect(@line_reader[1]).to eq(@pipes_messages[1])
      end

      it '[0] should be empty' do
        read
        expect(@line_reader[0]).to be_empty
      end
    end

    describe '#eof?' do
      subject { @line_reader.eof? }
      context 'all stream are still opened' do
        it { is_expected.to be_falsy }
      end

      context 'one stream is is still opened' do
        before(:example) { @pipe0_w.close }
        it { is_expected.to be_falsy }
      end

      context 'both streams are closed' do
        before(:example) do
          @pipe0_w.close
          @pipe1_w.close
        end
        it { is_expected.to be_truthy }
      end
    end

    describe '#read' do
      before(:example) do
        @pipes_messages[0].each { |line| @pipe0_w.puts line }
        @pipes_messages[1].each { |line| @pipe1_w.write "#{line}\r" }
      end
      subject(:read) do
        @line_reader.read
      end

      context 'both pipes opened' do
        it 'should block' do
          expect { Timeout.timeout(1) { read } }.to raise_exception(Timeout::Error)
        end
      end
      context 'one pipe still opened' do
        before(:example) do
          @pipe1_w.close
        end

        it 'should block' do
          expect { Timeout.timeout(1) { read } }.to raise_exception(Timeout::Error)
        end
      end
    end

    describe '#[]' do
      before(:example) { subject }

      it 'should return read lines for first stream' do
        expect(@line_reader[0]).to eq(@pipes_messages[0])
      end

      it 'should return read lines for second stream' do
        expect(@line_reader[1]).to eq(@pipes_messages[1])
      end

      it 'should return nil outside bound' do
        expect(@line_reader[2]).not_to be
      end
    end

    context 'with extra \r at the end of the line' do
      subject do
        Thread.new do
          @pipes_messages[0].each { |line| @pipe0_w.write "#{line}\n\r" }
          @pipes_messages[1].each { |line| @pipe1_w.write "#{line}\r\r" }
          @pipe0_w.close
          @pipe1_w.close
        end
        @line_reader.read
      end

      describe '#[]' do
        before(:example) { subject }

        it 'should return read lines for first stream' do
          expect(@line_reader[0]).to eq(@pipes_messages[0])
        end

        it 'should return read lines for second stream' do
          expect(@line_reader[1]).to eq(@pipes_messages[1])
        end

        it 'should return nil outside bound' do
          expect(@line_reader[2]).not_to be
        end
      end
    end

    context 'with extra \n at the end of the line' do
      subject do
        Thread.new do
          @pipes_messages[0].each { |line| @pipe0_w.write "#{line}\n\n" }
          @pipes_messages[1].each { |line| @pipe1_w.write "#{line}\r\n" }
          sleep 0.2
          @pipe0_w.close
          @pipe1_w.close
        end
        @line_reader.read
      end

      describe '#[]' do
        before(:example) { subject }

        it 'should return read lines for first stream' do
          expect(@line_reader[0]).to eq(@pipes_messages[0])
        end

        it 'should return read lines for second stream' do
          expect(@line_reader[1]).to eq(@pipes_messages[1])
        end

        it 'should return nil outside bound' do
          expect(@line_reader[2]).not_to be
        end
      end
    end

    context 'with multiple lines at once' do
      subject do
        Thread.new do
          @pipe0_w.write(@pipes_messages[0].join("\n"))
          @pipe1_w.write(@pipes_messages[1].join("\n"))
          @pipe0_w.close
          @pipe1_w.close
        end
        @line_reader.read
      end

      describe '#[]' do
        before(:example) { subject }

        it 'should return read lines for first stream' do
          expect(@line_reader[0]).to eq(@pipes_messages[0])
        end

        it 'should return read lines for second stream' do
          expect(@line_reader[1]).to eq(@pipes_messages[1])
        end

        it 'should return nil outside bound' do
          expect(@line_reader[2]).not_to be
        end
      end
    end
  end
end
