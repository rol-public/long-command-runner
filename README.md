# Long Command Runner

[![pipeline status](https://gitlab.com/rol_public/long-command-runner/badges/master/pipeline.svg)](https://gitlab.com/rol_public/long-command-runner/commits/master)
[![Gem Version](https://badge.fury.io/rb/long-command-runner.svg)](https://badge.fury.io/rb/long-command-runner)
[![coverage report](https://gitlab.com/rol_public/long-command-runner/badges/master/coverage.svg)](https://gitlab.com/rol_public/long-command-runner/commits/master)

This lib aims to follow a terminal command that is long to execute and monitor
the CPU usage and the progression of the program.

# Current state and goal

## Currently (what we have)
- monitoring of the progression of the application through the output of the app
    (I don't think that we can do better)
- get the real/user/sys time usage of the application by using the lib Benchmark of ruby.

## v1 goal

- Make it pure ruby solution not depending on `bash`
- Realtime mesure of cputime usage

# Install and usage

Install on your system:

    gem install long-command-runner

Install through bundler:

    gem 'long-command-runner', , '~> 0.1'

To use the library:

```ruby
require 'long_command_runner'

runner = LCR::Runner.new 'for i in `seq 100`; do echo $i%; sleep 1; done'
runner.launch

# command now runs

while runner.running?
  STDOUT.write "\rProgram is at: #{runner.progress} %   \r"
  STDOUT.flush
end
puts 'program ended !'
```

# documentation

Here some links:

 - [The documentation](https://rol-public.gitlab.io/long-command-runner/)
 - [The coverage](https://rol-public.gitlab.io/long-command-runner/coverage)
 - [The code quality report](https://rol-public.gitlab.io/long-command-runner/rubycritic/overview.html)
