# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift lib unless $LOAD_PATH.include?(lib)

require 'long-command-runner/version'

# rubocop:disable Metrics/BlockLength
Gem::Specification.new do |s|
  s.name        = 'long-command-runner'
  s.version     = LCR::VERSION
  s.date        = Time.now.utc.strftime('%Y-%m-%d')
  s.summary     = 'Long Command Runner'
  s.description =
    'This lib aims to follow a terminal command that is long to execute and monitor'\
    'the CPU usage and the progression of the program.'
  s.authors     = ['Roland Laurès']
  s.email       = 'roland@rlaures.pro'
  s.files       = Dir.glob('{lib}/**/*') + %w[LICENSE README.md]
  s.homepage    = 'https://gitlab.com/rol-public/long-command-runner'
  s.license     = 'GNU GPLv3'
  s.require_path = 'lib'
  s.metadata = {
    'bug_tracker_uri' => 'https://gitlab.com/rol-public/long-command-runner/issues',
    'documentation_uri' => 'https://rol-public.gitlab.io/long-command-runner',
    'homepage_uri' => 'https://gitlab.com/rol-public/long-command-runner',
    'mailing_list_uri' => 'https://gitlab.com/rol-public/long-command-runner/issues/service_desk',
    'source_code_uri' => 'https://gitlab.com/rol-public/long-command-runner/tree/master',
    'wiki_uri' => 'https://gitlab.com/rol-public/long-command-runner/wikis/home'
  }

  s.required_ruby_version = '>= 2.4'

  s.add_development_dependency 'pry', '~> 0.12'
  s.add_development_dependency 'rb-readline', '~> 0.5'
  s.add_development_dependency 'rspec', '~> 3.8'
  s.add_development_dependency 'rubocop', '~> 0.55'
  s.add_development_dependency 'rubycritic', '~> 3.5'
  s.add_development_dependency 'simplecov', '~> 0.16'
  s.add_development_dependency 'yard', '~> 0.9'

  s.requirements << 'bash (to use `time`)'
end
# rubocop:enable Metrics/BlockLength
